extends KinematicBody

# This script demonstrates how to rotate a character using the transform.basis
# property and the slerp function. A detailed explanation can be found at
# https://victorkarp.wordpress.com/?p=5611

var target_location: Vector3
var target_rotation: Basis
var rotation_lerp:= 0.0
var rotation_speed:= 0.25
var facing_angle = 0.0
var allow_tilting = false


func _ready():
	# Detach the target from the player, otherwise it would rotate when the
	# character rotates
	$Target.set_as_toplevel(true)
	find_new_target()


func _process(delta):
	if not allow_tilting:
		target_location.y = transform.origin.y
	if target_location != transform.origin:
		target_rotation = transform.looking_at(target_location, Vector3.UP).basis
		rotate_player(delta)


func set_target_location(new_target):
	target_location = new_target + global_transform.origin
	rotation_lerp = 0
	$Target.transform.origin = target_location


func rotate_player(delta):
	if rotation_lerp < 1:
		rotation_lerp += delta * rotation_speed
	elif rotation_lerp > 1:
		rotation_lerp = 1
	transform.basis = transform.basis.slerp(target_rotation, rotation_lerp).orthonormalized()


func _on_Timer_timeout():
	find_new_target()


func find_new_target():
	# Finds a new target at a random position around the character
	var rand_x = rand_range(-2,2)
	var rand_y = rand_range(0,2)
	var rand_z = rand_range(-2,2)
	var random_location = Vector3(rand_x, rand_y, rand_z)
	set_target_location(random_location)
